# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('article_headline', models.CharField(max_length=100)),
                ('article_content', models.TextField()),
                ('article_pub_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]

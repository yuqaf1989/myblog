from django.db import models

# Create your models here.

class Article(models.Model):
	article_headline = models.CharField(max_length=100)
	article_content = models.TextField()
	article_pub_date = models.DateTimeField(auto_now_add=True)
	
	def __unicode__(self):
		return self.article_headline




	